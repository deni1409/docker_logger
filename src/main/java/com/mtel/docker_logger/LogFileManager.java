package com.mtel.docker_logger;


import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

class LogFileManager {
    private final Configuration config;
    private File file;
    BufferedWriter writer;

    LogFileManager(Configuration config){
        this.config = config;
        getLastCreatedFile();
    }

    void log(String data) throws IOException, InterruptedException {
        if(file == null){
            System.out.println("null");
                write(data, createFile(formatDateTime()));
        }else{
            if((file.length()/1024/1024) >= config.getMaxFileSize()) {
                write(data, createFile(formatDateTime()));
            }else{
                write(data, file);
            }
        }
    }

    private void write(String data, File createdFile) throws IOException, InterruptedException {

        String texttobewrittentofile = data + System.lineSeparator();

//        FileReader fr = null;
//        BufferedReader br = null;
//
//        try {
//            fr = new FileReader(createdFile);
//            br = new BufferedReader(fr);
//
//            // do something..
//
//        } finally {
//            if (br != null) br.close();
//            if (fr != null) fr.close();
//        }

        System.out.println("Can write: " + createdFile.canWrite());
        FileOutputStream o = new FileOutputStream(createdFile,true);
        o.write(texttobewrittentofile.getBytes());
        o.flush();
        o.close();
//        writer = Files.newBufferedWriter(createdFile.toPath(), StandardCharsets.UTF_8,
//                StandardOpenOption.APPEND, StandardOpenOption.CREATE);
//
//        writer.write(texttobewrittentofile);
//
//        writer.close();
    }

    private void getLastCreatedFile() {


        file = Arrays.stream(Objects.requireNonNull(new File(config.getOutputFileLocation()).list()))
                .filter(file -> !file.equals("InternalErrors.log"))
                .reduce((first, second) -> second)
                .map(fileName -> new File(config.getOutputFileLocation() + fileName))
                .orElse(null);
    }

    private File createFile(String name) throws InterruptedException, IOException {

        File path = new File(config.getOutputFileLocation());

        if (path.list() != null){
            Arrays.stream(Objects.requireNonNull(path.list()))
                    .filter(file -> !file.equals("InternalErrors.log"))
                    .sorted(Collections.reverseOrder())
                    .skip(config.getStoredFilesLimit() - 1)
                    .forEach(file -> {
                        boolean isCreated = new File(config.getOutputFileLocation() + file).delete();
                        System.out.println(isCreated);
                    });
        }

//        Path paths = Paths.get(config.getOutputFileLocation());
//
//        Files.list(paths)
//                .filter(file -> !file.getFileName().toString().equals("InternalErrors.log"))
//                .sorted(Collections.reverseOrder())
//                .skip(config.getStoredFilesLimit() - 1)
//                .forEach(path -> path.toFile().delete());


        file = new File(config.getOutputFileLocation() + name + ".log");
        System.out.println(file.createNewFile());
        Thread.sleep(500);
        return file;
    }

    private String formatDateTime(){
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
        return date.format(formatter);
    }

    void logInternalError(String data) throws IOException {
        String text = LocalDateTime.now() + ": " + data + System.lineSeparator();
        if(!Files.exists(Paths.get(config.getOutputFileLocation() + "InternalErrors.log"))){
            Path file = Files.createFile(Paths.get(config.getOutputFileLocation() + "InternalErrors.log"));
            Files.write(file ,text.getBytes(),StandardOpenOption.APPEND , StandardOpenOption.CREATE);
        }else{
            Files.write(Paths.get(config.getOutputFileLocation() + "InternalErrors.log") ,text.getBytes(),StandardOpenOption.APPEND , StandardOpenOption.CREATE);
        }
    }
}