package com.mtel.docker_logger;

import java.awt.*;
import java.util.Scanner;

public class CommandsReader {
    static Scanner scanner = new Scanner(System.in);
    private static String CHANGE_ACCEPT_TYPE = "change-accept";
    private static String CHANGE_MAX_FILE_SIZE = "change-max-size";
    static String CHANGE_LOG_FILES_LIMIT = "change-files-limit";
    static String HELP = "help";

    public static void startReader(Configuration config){

        String command = scanner.next();
        if (command.equals(CHANGE_ACCEPT_TYPE)){
            System.out.println("Enter new Accept type value: ");
            config.setAcceptPropertyValue(scanner.next());
        }else if (command.equals(CHANGE_MAX_FILE_SIZE)){
            System.out.println("Enter new Max file size value: ");
            config.setMaxFileSize(scanner.nextInt());
        }else if (command.equals(CHANGE_LOG_FILES_LIMIT)){
            System.out.println("Enter new Log files limit");
            config.setStoredFilesLimit(scanner.nextInt());
        }else if (command.equals(HELP)){
            System.out.println("    -Enter new Accept type value: " + CHANGE_ACCEPT_TYPE);
            System.out.println("    -Enter new Max file size value: " + CHANGE_MAX_FILE_SIZE);
            System.out.println("    -Enter new Log files limit: " + CHANGE_LOG_FILES_LIMIT);
        }else {
            System.out.println("Please enter valid command!");
            System.out.println("Enter 'help' to see existing commands");
        }

        startReader(config);
    }
}
