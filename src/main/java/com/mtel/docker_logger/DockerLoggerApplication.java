package com.mtel.docker_logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerLoggerApplication {
	public static void main(String[] args) {
		SpringApplication.run(DockerLoggerApplication.class, args);
		Configuration configuration = new Configuration();
		configuration.loadConfiguration(args[0]);
		new RequestStream(configuration).connect();
	}
}
