package com.mtel.docker_logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestStream {

    private final Configuration configuration;
    LogFileManager logFileManager = null;

    public RequestStream(Configuration configuration) {
        this.configuration = configuration;
    }

    void connect(){
        try {
            URL url = new URL(configuration.getSeverUrl());
            HttpURLConnection connnection = (HttpURLConnection) url.openConnection();
            connnection.setRequestMethod("GET");
            if (configuration.getAcceptPropertyValue() != null) {
                connnection.setRequestProperty("Accept", configuration.getAcceptPropertyValue());
            }
            connnection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(connnection.getInputStream()));
            String decodedString;

            while ((decodedString = in.readLine())  != null) {
                if (logFileManager == null){
                    logFileManager = new LogFileManager(configuration);
                }
                logFileManager.log(decodedString);
            }
            in.close();
        } catch (IOException | InterruptedException e) {
            if (logFileManager == null){
                logFileManager = new LogFileManager(configuration);
            }
            try {
                e.printStackTrace();
                logFileManager.logInternalError(e.getMessage());
                Thread.sleep(5000);
            } catch (InterruptedException | IOException e1) {
                System.out.println(e1.getMessage());
            }
            connect();
        }
    }

}
