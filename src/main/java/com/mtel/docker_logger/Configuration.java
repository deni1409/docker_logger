package com.mtel.docker_logger;

import java.io.*;
import java.util.Properties;

public class Configuration {

    private float maxFileSize;
    private String severUrl;
    private String outputFileLocation;
    private String acceptPropertyValue;
    private int storedFilesLimit;

    public int getStoredFilesLimit() {
        return storedFilesLimit;
    }

    public void setStoredFilesLimit(int storedFilesLimit) {
        this.storedFilesLimit = storedFilesLimit;
    }

    public String getAcceptPropertyValue() {
        return acceptPropertyValue;
    }

    public void setAcceptPropertyValue(String acceptPropertyValue) {
        this.acceptPropertyValue = acceptPropertyValue;
    }

    public float getMaxFileSize() {
        return maxFileSize;
    }

    public void setMaxFileSize(int maxFileSize) {
        this.maxFileSize = maxFileSize;
    }

    public String getSeverUrl() {
        return severUrl;
    }

    public void setSeverUrl(String severUrl) {
        this.severUrl = severUrl;
    }

    public String getOutputFileLocation() {
        return outputFileLocation;
    }

    public void setOutputFileLocation(String outputFileLocation) {
        this.outputFileLocation = outputFileLocation;
    }

    public void loadConfiguration(String filePath){
        Properties p = new Properties();
        FileInputStream fIs = null;
        try {
            fIs = new FileInputStream(new File(filePath));
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        try {
            p.load(fIs);
            maxFileSize = Float.parseFloat(p.getProperty("MAX-FILE-SIZE"));
            severUrl = p.getProperty("SERVER-URL");
            outputFileLocation = p.getProperty("OUTPUT-FILE-LOCATION");
            acceptPropertyValue = p.getProperty("PROPERTY-ACCEPT",null);
            storedFilesLimit = Integer.parseInt(p.getProperty("FILES-LIMIT", 0 + ""));

        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
